package com.sda.builder;

public class Coffee {
    private boolean baseCoffee;
    private boolean chocolate;
    private boolean milk;
    private boolean syrup;
    private boolean sugar;

    public Coffee(boolean chocolate, boolean milk, boolean syrup, boolean sugar) {
        this.baseCoffee = true;
        this.chocolate = chocolate;
        this.milk = milk;
        this.syrup = syrup;
        this.sugar = sugar;
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "baseCoffee=" + baseCoffee +
                ", chocolate=" + chocolate +
                ", milk=" + milk +
                ", syrup=" + syrup +
                ", sugar=" + sugar +
                '}';
    }
}
