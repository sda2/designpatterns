package com.sda.builder;

public class Main {
    public static void main(String... args) {

        /**
         * Coffee vending using the builder pattern
         * */

        Coffee milkyCoffee = new CoffeeBuilder()
                .withMilk()
                .withSugar()
                .build();

        Coffee chocolateCoffee = new CoffeeBuilder()
                .withChocolate()
                .withSyrup()
                .build();

        Coffee chocoMilkCoffee = new CoffeeBuilder()
                .withChocolate()
                .withMilk()
                .withSyrup()
                .build();

        System.out.println(milkyCoffee.toString());
        System.out.println(chocolateCoffee.toString());
        System.out.println(chocoMilkCoffee.toString());

    }
}
