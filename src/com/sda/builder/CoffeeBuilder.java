package com.sda.builder;

public class CoffeeBuilder {

    private boolean chocolate;
    private boolean milk;
    private boolean syrup;
    private boolean sugar;

    public CoffeeBuilder withChocolate() {
        this.chocolate = true;
        return this;
    }

    public CoffeeBuilder withMilk() {
        this.milk = true;
        return this;
    }

    public CoffeeBuilder withSyrup() {
        this.syrup = true;
        return this;
    }

    public CoffeeBuilder withSugar() {
        this.sugar = true;
        return this;
    }

    Coffee build() {
        return new Coffee(this.chocolate, this.milk, this.syrup, this.sugar);
    }
}
