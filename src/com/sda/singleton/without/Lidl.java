package com.sda.singleton.without;

public class Lidl {
    private String drinksLocation = "North East";

    public String getDrinksLocation() {
        return drinksLocation;
    }

    public void setDrinksLocation(String drinksLocation) {
        this.drinksLocation = drinksLocation;
    }
}
