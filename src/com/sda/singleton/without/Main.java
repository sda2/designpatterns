package com.sda.singleton.without;

public class Main {
    public static void main(String... args) {

        /**
         * Lidl is a store that does not apply the singleton pattern
         * */

        /** Here we created 2 different instances of Lidl*/
        Lidl lidlInTheMorning = new Lidl();
        Lidl lidlInTheAfternoon = new Lidl();

        lidlInTheMorning.setDrinksLocation("South West");

        /** Here, we try to check the location of drinks in the lidl in the morning and in the afternoon*/
        System.out.printf("The Location of Drinks in lidlInTheMorning is: '%s' %n", lidlInTheMorning.getDrinksLocation());
        System.out.printf("The Location of Drinks in lidlInTheAfternoon is: '%s' %n%n", lidlInTheAfternoon.getDrinksLocation());

        System.out.println(lidlInTheMorning);
        System.out.println(lidlInTheAfternoon);
    }
}
