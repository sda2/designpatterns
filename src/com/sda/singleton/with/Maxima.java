package com.sda.singleton.with;

public class Maxima {
    private static Maxima instance = null;
    private String drinkLocation = "South West";

    private Maxima() {

    }

    public static Maxima getInstance() {
        if (instance == null) {
            instance = new Maxima();
        }

        return instance;
    }

    public String getDrinkLocation() {
        return drinkLocation;
    }

    public void setDrinkLocation(String drinkLocation) {
        this.drinkLocation = drinkLocation;
    }
}
