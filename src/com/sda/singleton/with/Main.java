package com.sda.singleton.with;

public class Main {
    public static void main(String... args) {

        /**
         * Maxima is a store that does apply the singleton pattern
         * */

        Maxima maximaInTheMorning = Maxima.getInstance();
        Maxima maximaInTheEvening = Maxima.getInstance();

        maximaInTheMorning.setDrinkLocation("South West");

        /** Here, we try to check the location of drinks in the maxima in the morning and in the evening*/
        System.out.printf("The Location of Drinks in maximaInTheEvening is: '%s' %n", maximaInTheMorning.getDrinkLocation());
        System.out.printf("The Location of Drinks in maximaInTheEvening is: '%s' %n%n", maximaInTheEvening.getDrinkLocation());

        System.out.println(maximaInTheMorning);
        System.out.println(maximaInTheEvening);

    }
}
